package com.example.ashwin.productsearchresults;

/**
 * Created by ashwin on 24/11/17.
 */

public class Product {

    private String title, price, image;

    public Product(String title, String price, String image) {
        this.title = title;
        this.price = price;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return title + " : " + price;
    }
}
