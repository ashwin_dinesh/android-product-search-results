package com.example.ashwin.productsearchresults;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by ashwin on 13/11/17.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductViewHolder> {

    private ArrayList<Product> mProductsList;
    private ImageLoader mImageLoader;

    public ProductsAdapter(ArrayList<Product> productsList) {
        mProductsList = productsList;
        mImageLoader = ImageLoader.getInstance();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        public TextView mTitleTextView, mPriceTextView;
        public ImageView mImageView;

        public ProductViewHolder(View view) {
            super(view);
            mTitleTextView = (TextView) view.findViewById(R.id.titleTextView);
            mPriceTextView = (TextView) view.findViewById(R.id.priceTextView);
            mImageView = (ImageView) view.findViewById(R.id.productImageView);
        }
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_product, parent, false);

        return new ProductViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, int position) {
        Product product = mProductsList.get(position);

        // Title
        (holder.mTitleTextView).setText(product.getTitle());

        // Price
        (holder.mPriceTextView).setText(product.getPrice());

        // Image
        mImageLoader.displayImage(product.getImage(), holder.mImageView);
    }

    @Override
    public int getItemCount() {
        return mProductsList.size();
    }

}
