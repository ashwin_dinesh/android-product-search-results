package com.example.ashwin.productsearchresults;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String BASE_URL = "https://www.snapdeal.com/search?keyword=";
    private String mUrl = BASE_URL;
    private String mQuery = "";
    private static final String TAG_STRING_REQUEST = "string_request";
    private boolean mIsSearching = false;
    private ArrayList<Product> mProductsList = new ArrayList<>();

    private SearchView mSearchView;
    private TextView mTextView;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private ProductsAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

    private void initViews() {
        mTextView = (TextView) findViewById(R.id.textView);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ProductsAdapter(mProductsList);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        mSearchView.setOnQueryTextListener(this);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo( getComponentName() ));
        mSearchView.setIconifiedByDefault(true);
        mSearchView.setQueryRefinementEnabled(true);
        mSearchView.setSubmitButtonEnabled(true);

        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String query = intent.getStringExtra(SearchManager.QUERY);
        Log.d("debuglogging", TAG + " : onNewIntent() : query : " + query);
        if (!mIsSearching) {
            mQuery = query;
            search(query);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    private void search(String query) {
        mUrl = BASE_URL + query;
        Log.d("debuglogging", TAG + " : search() : url : " + mUrl);
        new AsyncTask<Void, Void, ArrayList<Product>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mIsSearching = true;
                updateAdapter(null);
            }

            @Override
            protected ArrayList<Product> doInBackground(Void... params) {
                try {
                    Document doc = Jsoup.connect(mUrl)
                            .header("Accept-Encoding", "gzip, deflate")
                            .userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0")
                            .maxBodySize(Integer.MAX_VALUE)
                            .get();

                    Elements products = doc.select("div.col-xs-6.favDp.product-tuple-listing.js-tuple");

                    ArrayList<Product> productsList = new ArrayList<>();

                    for (Element product : products) {
                        if (isCancelled()) {
                            return null;
                        }

                        String title = "", price = "", image = "";

                        Elements elements = product.getElementsByTag("source");
                        for (Element e : elements) {
                            image = e.attr("srcset");
                        }

                        elements = product.getElementsByClass("product-title");
                        for (Element e : elements) {
                            title = e.text();
                        }

                        elements = product.getElementsByClass("lfloat product-price");
                        for (Element e : elements) {
                            price = e.text();
                        }

                        productsList.add(new Product(title, price, image));
                        String p = title + " : " + price + " : " + image + "\n";
                        Log.d("debuglogging", TAG + " : onResponse() : product : " + p);
                    }

                    Log.d("debuglogging", TAG + " : onResponse() : total products : " + productsList.size());

                    return productsList;
                } catch (Exception e) {
                    Log.e("debuglogging", TAG + " : search() : exception : " + e.toString());
                }

                return null;
            }

            @Override
            protected void onPostExecute(ArrayList<Product> productsList) {
                super.onPostExecute(productsList);
                mIsSearching = false;
                updateAdapter(productsList);
            }
        }.execute();

    }

    private void updateAdapter(ArrayList list) {
        if (list == null) {
            if (mIsSearching) {
                mTextView.setText("Loading...");
            } else {
                mTextView.setText("Error, please retry.");
            }
            mRecyclerView.setVisibility(View.GONE);
            mTextView.setVisibility(View.VISIBLE);
            return;
        }

        if (list.isEmpty()) {
            mTextView.setText("No result found");
            mTextView.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            return;
        }

        mProductsList = new ArrayList<>(list);
        mAdapter = new ProductsAdapter(list);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setVisibility(View.VISIBLE);
        mTextView.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
